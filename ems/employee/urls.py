from django.conf.urls import url
from . import views
from employee.views import *

urlpatterns = [
    url(r'^add/',views.employee_add, name='employee_add'),
    url(r'^(?P<id>[0-9]+)/edit/',views.employee_edit, name='employee_edit'),
    url(r'^$',views.employee_list, name='employee_list'),
    url(r'^(?P<id>[0-9]+)/detail/',views.employee_details, name='employee_details'),
    url(r'^(?P<id>[0-9]+)/delete/',views.employee_delete, name='employee_delete'),
    # url(r'^login/', views.user_login, name="user_login"),
    url(r'^success/', views.success, name="user_success"),
    # url(r'^logout/', views.user_logout, name="user_logout")
    
]
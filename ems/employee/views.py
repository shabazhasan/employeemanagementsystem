from django.shortcuts import render
from employee.models import *
from forms import *
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.views.generic import DetailView, ListView
from django.views.generic.edit import UpdateView
from django.core.urlresolvers import reverse_lazy





# def index(request):
#     print ("index called")
#     return HttpResponseRedirect(reverse('employee_list'))


def user_login(request):
    print ("user called")
    context = {}
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            if request.GET.get('next', None):
                return HttpResponseRedirect(request.GET['next'])
            return HttpResponseRedirect(reverse('employee_list'))
        else:
            context["error"] = "Provide valid credentials !!"
            return render(request, "auth/login.html", context)
    else:
        return render(request, "auth/login.html", context)

def success(request):
    context = {}
    context['user'] = request.user
    return render(request, "auth/success.html", context)

def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse("user_login"))

# @login_required(login_url="/login/")
def employee_list(request):
    context = {}
    context['users'] = User.objects.all()
    context['title'] = "Employees"
    return render(request, "employee/index.html", context)

# @login_required(login_url="/login/")
def employee_details(request, id=None):
    context = {}
    context['user'] = get_object_or_404(User, id=id)
    return render(request,"employee/details.html",context)

def employee_add(request):
    context = {}
    if request.method =="POST":
        user_form = UserForm(request.POST)
        context['user_form'] = user_form
        if user_form.is_valid():
            user_form.save()
            return HttpResponseRedirect(reverse("employee_list"))
        else:
            return render(request, 'employee/add.html', context)
    
    else:
        user_form = UserForm()
        context['user_form'] = user_form
        return render(request, 'employee/add.html', context)

def employee_edit(request, id=None):
    user = get_object_or_404(User, id=id)
    if request.method == "POST":
        user_form = UserForm(request.POST, instance=user)
        if user_form.is_valid():
            user_form.save()
            return HttpResponseRedirect(reverse('employee_list'))
        else:
            return render(request, 'employee/edit.html', {"user_form": user_form})
    else:
        user_form = UserForm(instance =user)
        return render(request, 'employee/edit.html', {"user_form": user_form})

def employee_delete(request, id=None):
    user= get_object_or_404(User,id=id)
    if request.method == "POST":
        user.delete()
        return HttpResponseRedirect(reverse("employee_list"))

    else:
        context = {}
        context['user'] = user
        return render(request,'employee/delete.html', context)


class ProfileUpdate(UpdateView):
    fields = ['designation', 'salary']
    template_name = 'auth/profile_update.html'
    success_url = reverse_lazy('my_profile')

    def get_object(self):
        print "update"
        return self.request.user.profile



class MyProfile(DetailView):
    template_name = 'auth/profile.html'

    def get_object(self):
        print "profile"
        return self.request.user.profile

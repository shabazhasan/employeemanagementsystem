"""ems URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url, include
from django.contrib import admin
from employee.views import user_login, user_logout, MyProfile , ProfileUpdate

urlpatterns = [
    # url('', index, name='home'),
    url(r'^admin/', admin.site.urls),
    url(r'^polls/',include('poll.urls')),
    url(r'^employee/',include('employee.urls')),
    url(r'^login/', user_login, name="user_login"),
    url(r'^logout/', user_logout, name="user_logout"),
    url(r'^profile/', MyProfile.as_view(), name="my_profile"),
    url(r'^profile/update', ProfileUpdate.as_view(), name="update_profile"),

]

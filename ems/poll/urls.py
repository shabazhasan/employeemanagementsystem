from django.conf.urls import url
from . import views
from poll.views import *

urlpatterns = [
    url(r'^add/', PollView.as_view(), name='poll_add'),
    url(r'^(?P<id>[0-9]+)/edit/', PollView.as_view(), name='poll_edit'),
    url(r'^(?P<id>[0-9]+)/delete/', PollView.as_view(), name='poll_delete'),
    url(r'^list/',views.index, name='poll_list'),
    url(r'^(?P<id>[0-9]+)/details/',views.details, name='poll_details'),
    url(r'^(?P<id>[0-9]+)/',views.vote_poll, name='poll_vote')
]